package com.company;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
	// write your code here
        HashMap<String, Integer>  car = new HashMap<>();

        car.put("BMW", 1);
        car.put("Tesla", 2);
        car.put("Volvo", 3);

        //print out all
        System.out.println(car);

        //Use loop print out all
        for (String i : car.keySet()) {
            System.out.println(i);
        }

        System.out.println(car.size());

        System.out.println(car.get("BMW"));
        car.remove("BMW");

        System.out.println(car);

        //only print the value without key
        System.out.println(car.values());
        car.clear();
        System.out.println(car);

        // isEmpty 
        System.out.println(car.isEmpty());

    }
}
